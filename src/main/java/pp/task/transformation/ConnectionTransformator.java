package pp.task.transformation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pp.task.annotations.Transformation;
import pp.task.dao.ConnectionDao;
import pp.task.dtos.FilterObject;
import pp.task.dtos.ConnectionDTO;
import pp.task.models.Connection;
import pp.task.utils.Constants;
import pp.task.utils.ObjectMapper;

/**
 * The purpose of this layer is to be completely independent on the database
 * Having this layer we can change the database model(use nosql db for example)
 * without changing anything in the service layer.
 * @author hangelov
 *
 */
@Transformation
public class ConnectionTransformator {
	
	@Autowired
	private ConnectionDao connectionDao;
	
	/**
	 * The transformation method for executing the business
	 * logic connected with the Connection action
	 * @param connectionDto
	 * @return
	 */
	public ConnectionDTO saveConnection(ConnectionDTO request) {
		Connection connectionToSave = new Connection();
		connectionToSave.setClientBrowser(request.getClientBrowser());
		connectionToSave.setClientIpAddress(request.getClientIpAddress());
		connectionDao.save(connectionToSave);
		Connection savedConnection = connectionDao.getById(connectionToSave.getId());
		if(savedConnection != null) {
			ConnectionDTO savedConnectionDTO = ObjectMapper.connectionToDTO(savedConnection);
			return savedConnectionDTO;
		}
		
		return null;
	}
	
	/**
	 * The connection method for acquiring Connection pages count 
	 * @return the connections' pages count
	 */
	public int getPagesCount() {
		int connectionsCount = connectionDao.getConnectionsCount();
		int pagesCount = (int) Math.ceil(connectionsCount / Constants.RESULTS_PER_PAGE);
		return pagesCount;
	}
	
	/**
	 * The transformation method for getting all connections on the specified page
	 * @param page we want to retrieve
	 * @return the found connections
	 */
	public List<ConnectionDTO> getConnectionsPerPage(int page) {
		List<Connection> connections = connectionDao.getConnectionsPerPage(page);
		List<ConnectionDTO> requests = ObjectMapper.connectionListToDtoList(connections);
		return requests;
	}
	
	/**
	 * Transformation service for getting the connections filtered by date
	 * @param filter the filter object with the date to filter by
	 * @return the found connections
	 */
	public List<ConnectionDTO> getConnectionsFilteredByDate(FilterObject filter) {
		List<Connection> connections = connectionDao.getConnectionsFilteredByDate(filter.getStartDate(), filter.getEndDate());
		List<ConnectionDTO> requests = ObjectMapper.connectionListToDtoList(connections);
		return requests;
	}
}
