package pp.task.controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pp.task.business.ConnectionService;
import pp.task.dtos.FilterObject;
import pp.task.dtos.ConnectionDTO;
import pp.task.utils.Constants;
import pp.task.utils.ObjectMapper;

/**
 * Communication service for the Connection related actions
 * @author hangelov
 *
 */

@Controller
public class ConnectionController {
	
	
	@Autowired
	private ConnectionService connectionService;
	
	/**
	 * Index page
	 * @return the index page
	 */
	@RequestMapping(value="/")
	String index() {
		return "index";
	}
	/**
	 * Action for saving the user connection
	 * @param params the params which are going to be displayed on view
	 * @param request The http request wrapper used for extracting client
	 * connection
	 * @return
	 */
	@RequestMapping(value="/connection", method=RequestMethod.GET)
    String saveConnection(Map<String, Object> params, HttpServletRequest request) {
		String userAgent = request.getHeader(Constants.USER_AGENT_HEADER);
		String connectionIpAddress = request.getRemoteAddr();
		ConnectionDTO connectionDto = ObjectMapper.userAgentToConnectionDTO(userAgent, connectionIpAddress);
		connectionDto = connectionService.saveConnection(connectionDto);
		params.put("connectionDto", connectionDto);
		
        return "connection";
    }
	
	/**
	 * Action for displaying all persisted connections
	 * @param page is the page to be displayed
	 * @param params the params which are going to be displayed on view
	 * @return
	 */
	@RequestMapping(value="/connections-list", method=RequestMethod.GET)
	String getAllConnections(@RequestParam(value = "page", defaultValue = Constants.FIRST_PAGE) String page, Map<String, Object> params) {
		int connectionPage = Integer.valueOf(page);
		int connectionsPagesCount = connectionService.getPagesCount();
		if(connectionPage > connectionsPagesCount) {
			return "error";
		}
		List<ConnectionDTO> connections = connectionService.getConnectionsPerPage(connectionPage);
		params.put("connections", connections);
		params.put("pagesCount", connectionsPagesCount);
		
		return "connections";
	}
	
	/**
	 * Action for filtering the connections by date
	 * @param filter is the FilterObject, consisting the date interval
	 * @param model SpringMvc model attribute
	 * @param params
	 * @return
	 */
	@RequestMapping(value="/connections-list", method=RequestMethod.POST)
	String filterConnectionsByDate(FilterObject filter, Model model, Map<String, Object> params) {
		List<ConnectionDTO> connections = connectionService.getConnectionsFilteredByDate(filter);
		params.put("connections", connections);
		
		return "connections";
	}

}
