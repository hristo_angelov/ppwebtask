package pp.task.utils;

import java.util.ArrayList;
import java.util.List;

import pp.task.dtos.ConnectionDTO;
import pp.task.models.Connection;

/**
 * Utility class used to map Entities to DTOs
 * @author hangelov
 *
 */
public class ObjectMapper {

	/**
	 * Create the connectionDto from the userAgent and the IP address
	 * @param userAgent the user agent retrieved from the request
	 * @param ipAddress the IP address of the client retrieved from the request
	 * @return
	 */
	public static ConnectionDTO userAgentToConnectionDTO(String userAgent, String ipAddress) {
		ConnectionDTO connectionDto = new ConnectionDTO();
		int userAgentHeaderLength = Constants.USER_AGENT_HEADER.length();
		String userBrowser = userAgent.substring(userAgentHeaderLength, userAgent.indexOf("("));
		connectionDto.setClientBrowser(userBrowser);
		connectionDto.setClientIpAddress(ipAddress);
		return connectionDto;
	}
	
	public static ConnectionDTO connectionToDTO(Connection connection) {
		ConnectionDTO connectionDTO = new ConnectionDTO();
		connectionDTO.setClientBrowser(connection.getClientBrowser());
		connectionDTO.setClientIpAddress(connection.getClientIpAddress());
		connectionDTO.setConnectionDate(connection.getCreateDate());
		return connectionDTO;
	}
	
	/**
	 * Map List<Connection> to List<ConnectionDTO>
	 * @param connections to be transformed
	 * @return the transformed List
	 */
	public static List<ConnectionDTO> connectionListToDtoList(List<Connection> connections) {
		List<ConnectionDTO> connectionsDtos = new ArrayList<ConnectionDTO>();
		for(Connection connection : connections) {
			ConnectionDTO connectionDto = new ConnectionDTO();
			connectionDto.setClientBrowser(connection.getClientBrowser());
			connectionDto.setClientIpAddress(connection.getClientIpAddress());
			connectionDto.setConnectionDate(connection.getCreateDate());
			connectionsDtos.add(connectionDto);
		}
		return connectionsDtos;

	}
}
