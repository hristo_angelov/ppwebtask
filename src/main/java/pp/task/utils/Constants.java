package pp.task.utils;

/**
 * Utility class used for declaring application constants
 * @author hangelov
 *
 */
public class Constants {

	public static final String USER_AGENT_HEADER = "User-Agent";
	public static final int RESULTS_PER_PAGE = 10;
	public static final String FIRST_PAGE = "1";
}
