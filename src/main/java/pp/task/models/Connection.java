package pp.task.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The connection Entity 
 * @author hangelov
 *
 */
@Entity
@Table(name = "connection")
public class Connection extends pp.task.models.Entity{
	
	@Column(name = "client_browser")
	private String clientBrowser;
	
	@Column(name = "client_ip_address")
	private String clientIpAddress;
	
	public String getClientBrowser() {
		return clientBrowser;
	}

	public void setClientBrowser(String clientBrowser) {
		this.clientBrowser = clientBrowser;
	}

	public String getClientIpAddress() {
		return clientIpAddress;
	}

	public void setClientIpAddress(String clientIpAddress) {
		this.clientIpAddress = clientIpAddress;
	}
	
}
