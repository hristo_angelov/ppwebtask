package pp.task.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import pp.task.models.Connection;
import pp.task.utils.Constants;

/**
 * The Connection DAO class used for maipulating the database
 * @author hangelov
 *
 */
@Repository
public class ConnectionDao extends GenericDao<Connection>{
	
	public ConnectionDao() {
		super(Connection.class);
	}
	
	
	/**
	 * Method used for getting all Connections from the database for the specified page
	 * @param page we want to retrieve
	 * @return
	 */
	public List<Connection> getConnectionsPerPage(int page) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Connection> query = builder.createQuery(Connection.class);
		Root<Connection> connectionRoot = query.from(Connection.class);
		query.select(connectionRoot);
		List<Connection> connections = entityManager.createQuery(query)
				.setFirstResult((page-1)*Constants.RESULTS_PER_PAGE)
				.setMaxResults(Constants.RESULTS_PER_PAGE).getResultList();
		return connections;
	}
	
	/**
	 * Method used for getting all Connections count
	 * @return
	 */
	public int getConnectionsCount() {
		Query query = entityManager.createQuery("select count(c.id) from Connection c");
		return (Integer)query.getSingleResult();
	}
	
	/**
	 * Method used for getting all Connections in the database filtered by Date
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<Connection> getConnectionsFilteredByDate(Date startDate, Date endDate) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Connection> query = builder.createQuery(Connection.class);
		Root<Connection> connectionRoot = query.from(Connection.class);
		Expression<Boolean> startDateExpr = builder.greaterThanOrEqualTo(connectionRoot.<Date>get("createDate"), startDate);
		Expression<Boolean> endDateExpr = builder.lessThanOrEqualTo(connectionRoot.<Date>get("createDate"), endDate);
		Expression<Boolean> andExpr = builder.and(startDateExpr, endDateExpr);
		query.select(connectionRoot);
		query.where(andExpr);
		return entityManager.createQuery(query).getResultList();
	}
}
