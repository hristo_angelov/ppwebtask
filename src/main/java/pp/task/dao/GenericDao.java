package pp.task.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import pp.task.models.Entity;

/**
 * This is the GenericDAO which is going to be extended from
 * all application DAO objects
 * @author hangelov
 *
 * @param <T>
 */
@Repository
public class GenericDao<T extends Entity> {

	protected EntityManager entityManager;
	
	private Class entityClass;
	
	GenericDao() {
	}
	
	protected GenericDao(Class entityClass) {
		this.entityClass = entityClass;
	}
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
	
	public void save(T entity) {
		this.entityManager.persist(entity);
	}
	
	public T getById(Long id) {
		return (T)entityManager.find(entityClass, id);
	}
	
}
