package pp.task.business;

import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pp.task.dtos.FilterObject;
import pp.task.dtos.ConnectionDTO;
import pp.task.transformation.ConnectionTransformator;
import pp.task.utils.Constants;

/**
 * The Business service for the Connection actions 
 * @author hangelov
 *
 */

@Service
public class ConnectionService {
	
	@Autowired
	private ConnectionTransformator connectionTransformator;
	
	/**
	 * The business method for executing the business
	 * logic connected with the Connection action
	 * @param connectionDto
	 * @return
	 */
	@Transactional
	public ConnectionDTO saveConnection(ConnectionDTO connectionDto) {
		ConnectionDTO savedConnection = connectionTransformator.saveConnection(connectionDto);
		return savedConnection;
	}
	
	/**
	 * The business method for acquiring Connection pages count 
	 * @return the connections' pages count
	 */
	@Transactional
	public int getPagesCount() {
		int connectionsCount = connectionTransformator.getPagesCount();
		int pagesCount = (int) Math.ceil(connectionsCount / Constants.RESULTS_PER_PAGE);
		return pagesCount;
	}
	
	/**
	 * The business method for getting all connections on the specified page
	 * @param page we want to retrieve
	 * @return the found connections
	 */
	@Transactional
	public List<ConnectionDTO> getConnectionsPerPage(int page) {
		List<ConnectionDTO> connections = connectionTransformator.getConnectionsPerPage(page);
		return connections;
	}
	
	/**
	 * Business service for getting the connections filtered by date
	 * @param filter the filter object with the date to filter by
	 * @return the found connections
	 */
	@Transactional
	public List<ConnectionDTO> getConnectionsFilteredByDate(FilterObject filter) {
		List<ConnectionDTO> connections = connectionTransformator.getConnectionsFilteredByDate(filter);
		return connections;
	}
}
