<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Connection page</title>
</head>
<body>
<table style="width:300px">
<tr>
  <th>Connection time</th>
  <th>Client browser</th> 
  <th>Client IP Address</th>		
</tr>
<tr>
  <td><c:out value="${requestDTO.connectionDate}" /></td>
  <td><c:out value="${requestDTO.clientBrowser}" /></td> 
  <td><c:out value="${requestDTO.clientIpAddress}" /></td>
</tr>
</table><br>
<a href="/web-task" >Index page</a>
</body>
</html>