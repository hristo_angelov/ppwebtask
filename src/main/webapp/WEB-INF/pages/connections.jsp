<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Connections list page</title>
</head>
<body>
<h1>Connections</h1><br>
<form:form action="connections-list" method="POST" modelAttribute="filter">
	<form:label path="startDate">Start date</form:label>
	<form:input path="startDate"/>
	<form:label path="endDate">End date</form:label>
	<form:input path="endtDate"/>
	
	<input type="submit" >
</form:form>
<table style="width:300px">
<tr>
  <th>Connection time</th>
  <th>Client browser</th> 
  <th>Client IP Address</th>		
</tr>
<c:forEach var="connection" items="$(connections)">
	<tr>
	  <td><c:out value="${connection.connectionDate}" /></td>
	  <td><c:out value="${requestDTO.clientBrowser}" /></td> 
	  <td><c:out value="${requestDTO.clientIpAddress}" /></td>
	</tr>
</c:forEach>
</table><br>
<c:forEach var="page" begin="1" end="$(pagesCount)">
   <a href="connections-list?page=$(page)">$(page)</a>&nbsp;
</c:forEach><br>
<a href="/web-task" >Index page</a>
</body>
</html>